const fs = require('node:fs/promises')

module.exports = {
  '!(**/dist/**/*){apps,packages}/**/*.{js,ts,css,jsx,tsx,cjs,mjs}': [
    'prettier -w',
  ],
  '{apps,packages}/**/src/**/*.{js,ts,jsx,tsx}': [
    (filenames) => {
      const appPaths = new Set(filenames.map((fn) => fn.split('src').at(0)))
      return [...appPaths].map((appPath) => `eslint --fix ${appPath}src/`)
    },
  ],
  '{apps,packages}/**/src/**/*.{ts,tsx}': [
    (filenames) => {
      const appPaths = new Set(
        filenames
          .map((fn) => fn.split('src').at(0))
          .filter(
            async (fn) =>
              await fs
                .access(fn + 'tsconfig.json', fs.constants.F_OK)
                .then(() => true)
                .catch(() => false),
          ),
      )
      return [...appPaths].map((appPath) => `tsc -p ${appPath} --noEmit`)
    },
  ],
  '*.{json,yml}': ['prettier -w'],
}
