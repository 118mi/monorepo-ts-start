import { IModule } from '@monorep/contracts'

export class ModuleDto {
  test: string

  constructor(parameters: IModule) {
    this.test = parameters.test
  }
}
