import { IModule } from '@monorep/contracts'
import { db } from 'db'
import { ModuleDto } from '../dtos/module.dto'
import { IModuleRepository } from '../entities/module.repository.interface'

class ModuleRepository implements IModuleRepository {
  async getById(id: string): Promise<IModule> {
    // because repository do not have bisnes logic
    const module = (await db.Module.findById(id).exec()) as IModule
    return new ModuleDto(module)
  }
}
