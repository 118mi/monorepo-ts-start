import { IModule } from '@monorep/contracts'
import { FastifyRequest } from 'fastify'
import { moduleUseCase } from '../use-cases/module.use-case'

export const moduleController = (req: FastifyRequest): Promise<IModule> => {
  return moduleUseCase()
}
