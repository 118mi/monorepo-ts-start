import { config } from '@monorep/configuration-provider'
import { CronJob } from 'cron'
import { moduleJobUseCase } from './use-cases/module-job.use-case'

class ModuleJob {
  start() {
    this.moduleJob.start()
  }

  stop() {
    this.moduleJob.stop()
  }

  private get moduleJob(): CronJob {
    if (!this._moduleJob) {
      this._moduleJob = new CronJob(config.get('moduleJob'), moduleJobUseCase)
    }
    return this._moduleJob
  }

  private _moduleJob?: CronJob
}

export const moduleJob = new ModuleJob()
