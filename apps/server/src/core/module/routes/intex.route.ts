import { FastifyPluginCallback } from 'fastify'
import { moduleController } from '../controllers/module.controller'

export const moduleRoute: FastifyPluginCallback = (fastify, _, done) => {
  fastify.register(moduleController, { prefix: 'custom' })
  done()
}
