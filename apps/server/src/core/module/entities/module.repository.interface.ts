import { IModule } from '@monorep/contracts'

export interface IModuleRepository {
  getById(id: string): Promise<IModule>
}
