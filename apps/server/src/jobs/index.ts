import { moduleJob } from 'core/module/module.job'

export const jobs = {
  start() {
    moduleJob.start()
  },
  stop() {
    moduleJob.stop()
  },
}
