import httpError from 'http-errors'
import { FastifyError } from 'fastify'
import { logger } from '@monorep/logger'

export const serverErrorHandler = (e: FastifyError) => {
  if (httpError.isHttpError(e) || e.validation) {
    return e
  }
  logger.error('Internal server error', e)
  return httpError.InternalServerError()
}
