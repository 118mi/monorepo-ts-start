import axios from 'axios'

interface IFormattedAxiosError {
  statusCode: number
  url: string
  method: string
  message: string
  statusText: string
  responseData: unknown
  headers: Partial<Record<string, string>>
}

class FormattedAxiosError implements IFormattedAxiosError {
  message: string
  statusCode: number
  url: string
  method: string
  statusText: string
  headers: Partial<Record<string, string>>
  responseData: unknown

  constructor(formattedAxiosError: IFormattedAxiosError) {
    this.statusCode = formattedAxiosError.statusCode
    this.message = formattedAxiosError.message
    this.statusText = formattedAxiosError.statusText
    this.responseData = formattedAxiosError.responseData
    this.url = formattedAxiosError.url
    this.headers = formattedAxiosError.headers
    this.method = formattedAxiosError.method
  }
}

const normalizeAxiosError = (e: unknown) => {
  if (!axios.isAxiosError(e)) return e
  const {
    status,
    config = {},
    data = {},
    statusText = '',
    headers = {},
  } = e.response || {}

  return new FormattedAxiosError({
    message: e.message,
    statusCode: status ?? NaN,
    statusText,
    responseData: data,
    headers,
    method: config.method ?? '',
    url: (config.baseURL ?? '') + (config.url ?? ''),
  })
}

export const axiosErrorNormalizer = (e: unknown) =>
  Promise.reject(normalizeAxiosError(e))

axios.interceptors.response.use(null, axiosErrorNormalizer)
