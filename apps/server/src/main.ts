import { server } from 'server'
import 'common/axios'
import { logger } from '@monorep/logger'
import { config } from '@monorep/configuration-provider'
import { db } from 'db'
import { jobs } from 'jobs'
import { config as JSONConfig } from 'config'

config.init(JSONConfig)
logger.init({
  prettyPrint: config.get('mode') === 'development',
})

const shutdown = async () => {
  try {
    await server.stop()
  } catch (e) {
    logger.error('Shutdown application error.', e)
  }
  try {
    await jobs.stop()
  } catch (e) {
    logger.error('Jobs stop error.', e)
  }
  try {
    await db.stop()
  } catch (e) {
    logger.error('Disconnect db error.', e)
  }
  process.exit(0)
}

if (config.get('mode') === 'production') {
  process.on('SIGTERM', shutdown)
  process.on('SIGINT', shutdown)
}

const main = async () => {
  try {
    await db.start()
    logger.info('Db was connected.')
  } catch (e) {
    logger.error('Connected db error.', e)
    return shutdown()
  }

  try {
    const address = await server.start()
    logger.info('Server running on: ' + address)
  } catch (e) {
    logger.error('Start application error.', e)
    return shutdown()
  }

  if (config.get('mode') === 'production') {
    try {
      await jobs.start()
      logger.info('Jobs was started.')
    } catch (e) {
      logger.error('Start jobs error', e)
      return shutdown()
    }
  }
}

main()
