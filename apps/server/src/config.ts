export const config = {
  mode: {
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  port: {
    format: 'port',
    default: 3001,
    env: 'PORT',
  },
  moduleJob: {
    format: String,
    default: '* * * * *',
    env: 'MODULE_JOB',
  },
}
