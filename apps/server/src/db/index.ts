import {
  Connection,
  createConnection as createConnectionCallback,
  disconnect as mongooseDisconnect,
  Model,
} from 'mongoose'
import { promisify } from 'node:util'
import { config } from '@monorep/configuration-provider'
import { IModule } from '@monorep/contracts'

const createConnection = promisify<string, Connection>(createConnectionCallback)

class DB {
  private module?: Model<IModule>

  get Module(): Model<IModule> {
    if (!this.module) this.createAndThrowShouldConnectError()
    return this.module
  }

  private createAndThrowShouldConnectError(): never {
    throw new Error('you should connect to db first')
  }

  async start() {
    const tgConn = await createConnection(config.get('dbTgUri'))
    this.module = tgConn.model<IModule>('Test', TestSchema, 'Test')
  }

  stop() {
    // return mongooseDisconnect()
  }
}

export const db = new DB()
