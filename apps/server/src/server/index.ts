import Fastify from 'fastify'
import cors from '@fastify/cors'
import { initRequestContext } from 'common/request-context-init.hook'
import { routes } from 'server/routes'
import { serverErrorHandler } from 'common/server-error-handler'
import { config } from '@monorep/configuration-provider'

const fastify = Fastify({ logger: false })
fastify.register(cors)

fastify.setErrorHandler(serverErrorHandler)

fastify.addHook('onRequest', initRequestContext)

fastify.register(routes)

export const server = {
  async stop() {
    return fastify.close()
  },

  async start(): Promise<string> {
    return new Promise((resolve, reject) => {
      fastify.listen(
        { port: config.get<number>('port') },
        function (err, address) {
          if (err) return reject(err)
          resolve(address)
        },
      )
    })
  },
}
