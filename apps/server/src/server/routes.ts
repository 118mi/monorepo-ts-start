import { FastifyPluginCallback } from 'fastify'

export const routes: FastifyPluginCallback = (fastify, _, done) => {
  fastify.register(
    (fastify, _, done) => {
      fastify.get('', () => 'test')
      done()
    },
    { prefix: '/api/v1' },
  )
  done()
}
