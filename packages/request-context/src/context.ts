import { AsyncLocalStorage } from 'node:async_hooks'

class Context {
  private readonly _context = new AsyncLocalStorage<Record<string, unknown>>()

  get store() {
    return this._context.getStore()
  }

  get context() {
    return this._context
  }

  setToStore(key: string, value: unknown) {
    const store = this.store
    if (!store) throw new Error('Init context first')
    store[key] = value
  }
}

export const context = new Context()
