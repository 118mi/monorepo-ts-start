module.exports = {
  ...require('@monorep/configs/eslint-server.cjs'),
  root: true,
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ['./tsconfig.json'],
  },
}
